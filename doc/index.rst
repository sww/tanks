.. tanks documentation master file, created by
   sphinx-quickstart on Thu Nov  7 15:49:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tanks's documentation!
=================================

Welcome to this minimal example on how to create your own python package. 

.. note:: This is how you can format a note.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   
   ./source/description
   ./source/installation
   ./source/example
   ./source/issues
   ./source/tanks


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
