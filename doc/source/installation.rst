.. _installation:

Installation
============

Get the latest version directly from GitLab using pip!

.. code-block:: bash

   $ pip3 install git+https://gitlab.switch.ch/sww/tanks.git


