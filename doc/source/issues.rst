Issues
======

**Any trouble with the package?**  
Leave me an issue on GitLab_

.. _GitLab: https://gitlab.switch.ch/sww/tanks
