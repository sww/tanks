# Description

This has been developed as part of an Tutorial! It is able to simulate basic interactions between water tanks!


Installation
------------

Get the latest version directly from gitlab using pip!

``pip3 install git+https://gitlab.switch.ch/sww/tanks.git``


Examples
--------

Leave some example code like ...

```python
import matplotlib.pyplot as plt
from tanks.tanks import Tank, initiate_tanks, get_levels

## define network and attributes 
network_structure = {"tank_6":["tank_4","tank_5"],"tank_4":["tank_1","tank_2"],"tank_5":["tank_3"]}
attributes = {"tank_1":(10,  0.0),
            "tank_2":(20,  0.06),
            "tank_3":(15,  0.9),
            "tank_4":(8,   0.3),
            "tank_5":(44,  0.5),
            "tank_6":(2.5, 0.0)}

## generate the network
all_tanks = initiate_tanks(network_structure,attributes)
final_tank = all_tanks["tank_6"] # only the last tank is necessary to run the entire simulation

## simulate 
levels = {"tank_{}".format(k):[] for k in range(7)[1:]} # init dict for our level values
for dt in range(100):
    tank_levels = get_levels(all_tanks) # retrieve levels for every iteration
    for tank_id, level in tank_levels.items(): # save the levels!
        levels[tank_id].append(level) # 
    final_tank.fill_tank() # open valves and let the water through to our last tank!


# checking results

fig, axis = plt.subplots(6,1,figsize = (12,8))
for idx, ax in enumerate(axis):
    ax.plot(levels["tank_{}".format(idx+1)])
    ax.set_title("tank_{}".format(idx+1))
plt.tight_layout()
plt.show()
```
