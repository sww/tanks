from setuptools import setup, find_packages

setup(name='tanks',
      version='1.0',
      description='A tool for simulating water tanks.',
      long_description='Our very long explanation on what our package does',
      classifiers=[
        'Development Status :: early stage',
        'Programming Language :: Python :: 3',
      ],
      install_requires=[], # this will normally be containg depencies from packages
      packages=find_packages(exclude=("doc",".git",".idea","venv", "test")),
      keywords='Water Management',
      author='Christian Foerster',
      author_email='Christian.Foerster@eawag.ch',
      license='MIT')
