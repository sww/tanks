# A tank is a object (-> class) we can touch,
# it has attributes like level and id and so on,
# discharge is property of a tank, furthermore it does have the capability to fill up (a function or method)
class Tank:    
    """Representation of a water tank with a footprint of 1 m²."""
    
    def __init__(self, tank_id, level, rate, upstream_tanks=None):
        """
        Parameters
        ----------
        tank_id : str
            Uniquely identifies tanks in your system.
        level : float
            Water level of a tank in meters [m].
        rate : float
            Specifies the changes in water level over time [1/s]. 
            Positive values denote a decrease in water level, negative values an increase.
            Used to calculate the discharge (level*rate).
        upstream_tanks : list
            Defines connections between tanks. 
            The provided list must contain entries of type Tank!
        
        Example
        -------
        # create a Tanke instance
        >>> Tank("id_1",1,0.1)
        <type Tank>
        tank_id: id_1
        level: 1
        rate: 0.1
        """
        
        self.tank_id = tank_id
        self.level = level
        self.rate = rate
        self.upstream_tanks = upstream_tanks
    
    # the method Q calculates the flow of a given tank every time it's called
    @property # allows use of the method without brackets 
    def Q(self):
        """
        Discharge of a tank. Product of level and rate.
        """
        return self.level * self.rate
    
    # filling a tank from all its upstream tanks
    def fill_tank(self):
        """
        Propagates the water through the system from the downstream to upstream.            
        """
        self.level -= self.Q
        if self.upstream_tanks is not None:
            for uptank in self.upstream_tanks:
                # recursion (sort of)
                self.level += uptank.Q
                uptank.fill_tank()
    
    # our joice of representation for a tank!
    # so that the user knows whats happening
    def __repr__(self):
        return "<type Tank>\ntank_id: {}\nlevel: {}\nrate: {}".format(self.tank_id,self.level,self.rate)


# in order to automatically init our tanks we will have to 
# provide the network structure and attributes of all tanks.
# a graph would be a good way to go. But we want to keep thinks nice and simple!
# lets go with some dictionaries, we could write adapters for graphs later...
# network_structure = {tank_down_i:[tank_up_i,tank_up_(i+1),...],} eg.: {6:[4,5],4:[1,2],5:[3]}
# attributes = {tank_id:(tank_level,  tank_rate),...}
def initiate_tanks(network_structure, attributes):
    """Creates all tanks in a network.

    Parameters
    ----------
    network_structure : dict
        Each entry describes the connection from one or more upstream tanks to one downstream tank.
        Entries must be in this shape:
        {..., tank_id_downstream: [..., tank_id_upstream_n, tank_id_upstream_np1, ...], ...}
    attributes : dict
        Each tank mentioned in the network_structure must have an entry here, specifying its level and rate.
        Entries must be in this shape:
        {..., tank_id: (level, rate), ...}
    
    Returns
    -------
    dict
        Containing all tanks. Dictionary keys are equal to tank_ids.
        
    Example
    -------
    
    # define network structure  
    # tank4 & tank5 flow into tank6, tank1 & tank2 flow into tank4, ... 
    >>> network_structure = {"tank_6":["tank_4","tank_5"],
                             "tank_4":["tank_1","tank_2"],
                             "tank_5":["tank_3"]}
    # define attributes 
    # tank_3 has a level on 15m and a rate of 0.9/s.
    >>> attributes = {"tank_1":(10,  0.0),
                      "tank_2":(20,  0.06),
                      "tank_3":(15,  0.9),
                      "tank_4":(8,   0.3),
                      "tank_5":(44,  0.5),
                      "tank_6":(2.5, 0.0)}

    # generate the network
    >>> all_tanks = initiate_tanks(network_structure,attributes)

    """

    # init all tanks without upstream tanks
    tanks = {}
    for tank_id in attributes:
        level = attributes[tank_id][0]
        rate = attributes[tank_id][1]
        tanks[tank_id] = Tank(tank_id, level, rate, upstream_tanks=None)
    # add upstream tanks
    for tank_id in network_structure:
        tanks[tank_id].upstream_tanks = [tanks[tank_id_up] for tank_id_up in network_structure[tank_id]]
    return tanks

# iterate over all tanks and retrieve these levels with id
def get_levels(tanks):
    """Extracts levels of provided tanks.
    
    Parameters
    ----------
    tanks : dict
        Containing tanks you want to extract levels of. Shape {..., tank_id: <tank instance>, ...}
    
    Returns
    -------
    dict
        Containing levels of all the tanks.
        
    Example
    -------
    
    # define tanks or generate networt with the 'initiate_tanks' function
    >>> tanks = {"id_0": Tank("id_0",10,0.1), "id_1": Tank("id_1",1,0.1)}
    
    # extract levels
    >>> levels = get_levels(tanks)
    """
    
    fill_dict={}
    for tank_id, tank in tanks.items():
        fill_dict[tank_id] = tank.level
        
    return fill_dict
