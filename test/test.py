### --- Tests ---- (those are normally in a sperate file, basic test example!)

import os
import sys
sys.path.append(os.path.abspath("."))

from tanks.tanks import Tank

# checking the calcualtion of discharge
tank = Tank(tank_id=1, level=10, rate=0.1)
assert tank.Q == 10*0.1, "The tank's discharge should be 0.3."
# checking the "leakage of a tank"
tank.fill_tank()
assert tank.level == 9, "The tank's level should be 9."

# checking when leakage 0
tank = Tank(tank_id=1, level=10, rate=0.00)
tank.fill_tank()
assert tank.level == 10, "The tank's level should be 10."

# checking mass balance with upstream tanks
tank_1 = Tank(tank_id=1, level=10, rate=0.1)
tank_2 = Tank(tank_id=2, level=50, rate=0.08)
tank_3 = Tank(tank_id=3, level=0, rate=0, upstream_tanks=[tank_1, tank_2])
tank_3.fill_tank()
assert tank_3.level == 10*0.1+50*0.08, "The tank's level should be 5."
assert tank_1.level == 10-1, "The tank's level should be 9."
assert tank_2.level == 50-4, "The tank's level should be 46."

# ...
# .
# .
# .


print("All tests passed!")
